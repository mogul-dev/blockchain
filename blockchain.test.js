const Blockchain = require('./blockchain');

const Block = require('./block');

describe('Blockchain', () =>{
    let blockchain, newChain, originalChain;
    console.log("orice")
   beforeEach(() => {
        console.log("Andrei", typeof blockchain);
        if( typeof blockchain == "undefined"){

        
        blockchain = new Blockchain();
        newChain = new Blockchain();
        console.log("Andrei", typeof blockchain);
        }
        originalChain = blockchain.chain;
    });

    it('contains a `chain` Array instance', () => {
        console.log("linia =================================================================21");
        expect(blockchain.chain instanceof Array).toBe(true);
        console.log("linia =================================================================23");
    });
    it('starts with the genesis block', () => {
        expect(blockchain.chain[0]).toEqual(Block.genesis());
    });
    it('adds a new block to the chain', () => {

        const newData = 'foo-bar';
        blockchain.addBlock({data: newData});

        expect(blockchain.chain[blockchain.chain.length-1].data).toEqual(newData);
    });

    describe('isValidChain ()', () => {
        describe('when the chain does not start with the genesis block', () => {
            it('returns false', () => {
                blockchain.chain[0]= {data : 'fake genesis block '};

                expect(Blockchain.isValidChain(blockchain.chain)).toBe(false);
            });
        });
        describe('when the chain starts with the genesis block and has multiple blocks ', () => {
            beforeEach(() => {
                blockchain.addBlock({ data: 'Bears'});
                blockchain.addBlock({ data: 'Beets'});
                blockchain.addBlock({ data: 'Battlestar Galactica'});
           });
            describe('and a lastHash reference has changed', () => {
                it('returns false',() => {
                    blockchain.chain[2].lastHash = 'broken lastHash';

                    expect(Blockchain.isValidChain(blockchain.chain)).toBe(false);
                });
            });
            describe('and the chain contains a block with an invalid field', () => {
                it('returns false', () => {
                    blockchain.chain[2].data = 'some-bad-and-evil-data';

                    expect(Blockchain.isValidChain(blockchain.chain)).toBe(false);
                });
            });
            describe('and the chain does not contain an invalid blocks', () => {
                it('returns true', () => { 
                   blockchain.chain[2].data = 'some-bad-and-evil-data';
                    console.log(blockchain);
                    console.log(Blockchain);
                    expect(Blockchain.isValidChain(blockchain.chain)).toBe(true);
                });
            });
        });
    });

    describe('replaceChain()', () => {
        describe('when the new chain is not longer', () => {
            it('does not replace the chain', () => {
                newChain.chain[0] = { new: 'chain'}; 
                blockchain.replaceChain(newChain.chain);

                expect(blockchain.chain).toEqual(originalChain);
            });
        });
        describe('when the new chain is longer', () => {
            beforeEach(() => {
                newChain.addBlock({ data: 'Bears'});
                newChain.addBlock({ data: 'Beets'});
                newChain.addBlock({ data: 'Battlestar Galactica'});
            });
            describe(' and the chain is invalid', () => {
                it('does not replace the chain', () => {
                    newChain.chain[2].hash = 'some-fake-hash';

                    blockchain.replaceChain(newChain.chain);

                    expect(blockchain.chain).toEqual(originalChain);
                });
            });
            describe('and the chain is valid', () => {
                it('replaces the chain', () => {

                    blockchain.replaceChain(newChain.chain);

                    expect(blockchain.chain).toEqual(newChain.chain);
                });
            })
        });
    });
});
